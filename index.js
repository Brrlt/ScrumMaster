const { Client, MessageAttachment } = require('discord.js');
const client = new Client();
const { token, bot, bottalk, genk } = require('./config.json');
const MIN_INTERVAL = 1000 * 60 * 15;
let currentdate = new Date();

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}

let cmsg = 0;
let count_jdr = false;
var score_joueur = [];
var score_des = [];
let i = 0;
let msg_c = 0;

client.once('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
    console.log(client);
});

client.on('message', msg => {
	if (count_jdr){
		if (msg.author.id != 627203649189183508) {
			if(msg.content === 'resume'){
				msg.channel.send("```json\n \" FIN DE LA PHASE D'INITIATIVE  \"```");
				for(i = 0; i < score_joueur.length; i++){
					msg.channel.send(score_joueur[i] + " - " + score_des[i]);
				}
				msg.channel.stopTyping();
				count_jdr = false;
			}else{
				msg.channel.startTyping();
				//msg.channel.send("message traité : " + msg_c++);
				if (msg.author.id == 279722369260453888){
					let occ = msg.content.lastIndexOf("#");
					score_des.push((msg.content).substring(occ+2, occ+4));
				}else{
					if (msg.content.includes('!d')){
						let occ = msg.content.lastIndexOf("!d");
						let guild = msg.guild;
						let member = guild.member(msg.author);
						let nickname = member ? member.displayName : null;
						score_joueur.push(nickname + " " + msg.content.substring(occ+5));
					}
				}
				msg.channel.stopTyping();
				/*for(let i = 0; i < score_joueur.length; i++){
					msg.channel.send(score_joueur[i] + " " + score_des[i]);
				}*/
			}

		}
	}
	if (msg.content === 'score'){
		msg.channel.startTyping();
		for(i = 0; i < score_joueur.length; i++){
			msg.channel.send(score_joueur[i] + " " + score_des[i]);
		}
		msg.channel.stopTyping();
	}
	if (msg.content === 'clean'){
		while (score_des.length) { score_des.pop(); }
		while (score_joueur.length) { score_joueur.pop(); }
	}

	if (msg.content === 'ping'){
		msg.reply('pong');
		cmsg++;
	}

	if (msg.content === ('!combien?')){
		msg.reply("Depuis que je suis démarré, j'ai envoyé : " + cmsg + " messages");
	}

	if (msg.content === 'salut'){
		msg.reply('Bonsoir très chèr(e)');
		cmsg++;
	}

	if(msg.content === 'What a story Mark!'){
		msg.channel.send("https://media.giphy.com/media/6oyHViE7fvjYk/source.gif");
		client.channels.cache.get(bottalk).send("https://media.giphy.com/media/6oyHViE7fvjYk/source.gif");
		cmsg++;
	}

	  if (msg.content.includes('fruit')){
		let r = getRandomInt(4)+1;
		var fruits = ['🍎', '🍇', '🍍', '🍏', '🍐', '🍊', '🍋', '🍌', '🍉', '🍓', '🍈', '🍒', '🍑', '🥭', '🥥', '🥝'];
	    for(let i = 0; i < r; i++){
			let r2 = getRandomInt(16);
			msg.react(fruits[r2]);
		}
	  }
	  if (msg.content.includes('split')){
		  if (msg.author.id != 627203649189183508) {
			   msg.channel.send("```ini\n[   " + (msg.content).substring(6) + "    ]```");
			   if (msg.content.includes('init')){
				   while (score_des.length) { score_des.pop(); }
				   while (score_joueur.length) { score_joueur.pop(); }
				   msg_c = 0;
				   count_jdr = true;
				   msg.channel.startTyping();
			   }

		  }
	  }
});

setInterval(function(){
	currentdate = new Date();
	if(currentdate.getHours() < 23 && currentdate.getHours() > 6 && currentdate.getHours()%2 == 0 && currentdate.getMinutes() > 0 && currentdate.getMinutes() < 16){
		client.channels.cache.get(bottalk).send('I\'m up ! at' + currentdate.getHours() + ' : ' + currentdate.getMinutes());
	}
}, MIN_INTERVAL);



client.login(token);
